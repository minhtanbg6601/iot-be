import { Module } from '@nestjs/common';
import { RoomService } from './room.service';
import { RoomController } from './room.controller';
import RoomRepository from 'src/shared/repos/room.repository';
import { MongooseModule } from '@nestjs/mongoose/dist/mongoose.module';
import { Room, RoomSchema } from 'src/shared/entities/room.entity';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { ConfigModule } from '@nestjs/config';
import { Participant, ParticipantSchema } from 'src/shared/entities/participant.entity';
import ParticipantRepository from 'src/shared/repos/participant.repository';

@Module({
  imports: [
    ConfigModule.forRoot(), MongooseModule.forFeature([{ name: Room.name, schema: RoomSchema }, { name: Participant.name, schema: ParticipantSchema }]),
    ConfigModule,
    ClientsModule.register([
      {
        name: 'MQTT_SERVICE',
        transport: Transport.MQTT,
        options: {
          url: (() => {
            return process.env.MQTT_URI
          })(),

        }
      },
    ]),

  ],
  controllers: [RoomController],
  providers: [ParticipantRepository,RoomService, RoomRepository]
})
export class RoomModule {
  constructor() { }
}
