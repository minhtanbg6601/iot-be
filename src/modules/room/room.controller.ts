import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { RoomService } from './room.service';
import { UpdateRoomDto } from './dto/update-room.dto';
import { IRoom } from 'src/shared/entities/room.entity';
import { DrawData } from './dto/draw.dto';
import { CurrentUser } from 'src/shared/decorators/user.decorator';
import { ParticipantDocument } from 'src/shared/entities/participant.entity';
import { UseAuthAndRolesSuperAdmin } from 'src/shared/guard/roles.guard';
import { Put } from '@nestjs/common/decorators';
import { Query } from 'mongoose';

@Controller('room')
export class RoomController {
  constructor(private readonly roomService: RoomService) { }

  @Post()
  @UseAuthAndRolesSuperAdmin()
  create(@Body() createRoomDto: IRoom, @CurrentUser() admin: ParticipantDocument) {
    return this.roomService.create(admin, createRoomDto);
  }

  @Get()
  findAll() {
    return this.roomService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') _id: string) {
    return this.roomService.findOne({ _id });
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateRoomDto: UpdateRoomDto) {
    return this.roomService.update(id, updateRoomDto);
  }

  @Delete(':id')
  @UseAuthAndRolesSuperAdmin()
  remove(@Param('id') id: string, @CurrentUser() admin: ParticipantDocument) {
    return this.roomService.remove(id, admin);
  }
  @Post('/draw/:id')
  @UseAuthAndRolesSuperAdmin()
  draw(@Param('id') id: string, @Body() drawData: DrawData, @CurrentUser() user: ParticipantDocument) {
    return this.roomService.draw(id, user, drawData);
  }
  @Post('/kick/:id')
  @UseAuthAndRolesSuperAdmin()
  kick(@Param('id') roomId: string, @CurrentUser() user: ParticipantDocument, @Body() { userId }) {
    return this.roomService.kickOutOfRoom(userId, roomId, user);
  }

  @Post('/allow-write/:id')
  @UseAuthAndRolesSuperAdmin()
  allowWrite(@Param('id') roomId: string, @Body() { userId }, @CurrentUser() user: ParticipantDocument) {
    return this.roomService.allowWrite({ roomId, userId }, user);
  }
  @Post('/join/:id')
  @UseAuthAndRolesSuperAdmin()
  joinRoom(@Param('id') roomId: string, @CurrentUser() user: ParticipantDocument) {
    return this.roomService.joinRoom(roomId, user);
  }
  // emptyBoard
  @Post('/clear/:id')
  @UseAuthAndRolesSuperAdmin()
  clearBoard(@Param('id') roomId: string, @CurrentUser() user: ParticipantDocument) {
    return this.roomService.emptyBoard(roomId, user);
  }
}
