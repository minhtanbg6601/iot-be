import { Injectable } from '@nestjs/common';
import { Inject } from '@nestjs/common/decorators';
import { ClientProxy } from '@nestjs/microservices';
import { FilterQuery } from 'mongoose';
import { EMQTTEvent } from 'src/shared/constants/mqtt.constant';
import { ParticipantDocument } from 'src/shared/entities/participant.entity';
import { IRoom, Room } from 'src/shared/entities/room.entity';
import RoomRepository from 'src/shared/repos/room.repository';
import { CreateRoomDto } from './dto/create-room.dto';
import { DrawData } from './dto/draw.dto';
import { UpdateRoomDto } from './dto/update-room.dto';
import { HttpException, HttpStatus } from '@nestjs/common';
import ParticipantRepository from 'src/shared/repos/participant.repository';
import { createCanvas, loadImage, Image } from "canvas";
import * as fastq from "fastq";
import type { queue, done } from "fastq";
import { flatMap } from 'rxjs';
import { log } from 'console';

@Injectable()
export class RoomService {
  private dataDigest: DrawDataIngester;
  constructor(private participantRepo: ParticipantRepository, private roomRepo: RoomRepository, @Inject('MQTT_SERVICE') private client: ClientProxy) {
    this.dataDigest = new DrawDataIngester(roomRepo, participantRepo)
  }


  findAll() {
    return this.roomRepo.findMany({});
  }

  findOne(dto: FilterQuery<Room>) {

    return this.roomRepo.findOne(dto);
  }
  async create(admin: ParticipantDocument, createRoomDto: CreateRoomDto) {
    const result = await this.roomRepo.createOne({ ...createRoomDto, admin: admin.id, currentWriter: admin.id, participant: [admin.toJSON() as ParticipantDocument] });
    const response = {
      ...result.toJSON()
    }

    this.client.emit(EMQTTEvent.CREATE_ROOM, response)
    return response
  }
  async update(id: string, updateRoomDto: UpdateRoomDto) {
    const result = await this.roomRepo.updateOne(id, updateRoomDto)
    if (!result) {
      throw new HttpException('not found', HttpStatus.NOT_FOUND)
    }
    console.log(EMQTTEvent.UPDATE_ROOM);

    this.client.emit(EMQTTEvent.UPDATE_ROOM, {
      id,
      updated: result.toJSON()
    })
    return result;
  }

  async remove(_id: string, user: ParticipantDocument) {
    const room = await this.roomRepo.findOne({ _id })
    if (room.admin !== user.id) {

    }
    await room.delete();
    this.client.emit(EMQTTEvent.DELETE_ROOM, {
      _id,
      deleted: room.toJSON()
    })
    return room.toJSON()
  }
  async draw(_id: string, user: ParticipantDocument, drawData: DrawData) {
    const room = await this.roomRepo.findOne({ _id })
    if (!room) {
      throw new HttpException("room not found", HttpStatus.NOT_FOUND)
    }
    if (room.currentWriter !== user.id && room.admin !== user.id) {
      throw new HttpException('not permitted', HttpStatus.BAD_REQUEST)
    }
    const response = {
      from: user._id.toString(),
      roomId: _id,
      ...drawData
    }
    console.log(EMQTTEvent.DRAW);
    // this.dataDigest.addJob(response)
    this.client.emit(EMQTTEvent.DRAW + response.roomId, response)
    return response
  }
  async kickOutOfRoom(userId: string, roomId: string, kicker: ParticipantDocument) {

    const room = await this.roomRepo.findOne({ _id: roomId })
    if (room.admin === userId) {
      throw new HttpException("cannot kick admin", HttpStatus.BAD_REQUEST)
    }
    if (userId !== kicker.id) {
      if (room.admin !== kicker.id) {
        throw new HttpException("not permited", HttpStatus.BAD_REQUEST)
      }
    }
    const response = {
      toKick: userId,
      roomId
    }
    room.participant = room.participant.filter(e => {
      return e._id.toString() !== userId
    });
    if (room.currentWriter === userId) {
      room.currentWriter = room.admin;
    }

    await room.save();
    this.client.emit(EMQTTEvent.KICK + response.roomId, response)
    return response;
  }
  async allowWrite({ userId, roomId }, admin: ParticipantDocument) {
    const room = await this.roomRepo.findOne({ _id: roomId })
    if (room.admin !== admin.id) {
      throw new HttpException("not allowed", HttpStatus.BAD_REQUEST)
    }
    room.currentWriter = userId;
    await room.save()
    this.client.emit(EMQTTEvent.CHOOSEN + roomId, { roomId, userId })
    return room.toJSON()
  }
  async joinRoom(roomId: string, user: ParticipantDocument) {
    const room = await this.roomRepo.findOne({ _id: roomId })

    const response = {
      user: user.toJSON(),
      roomId
    }
    if (room.participant.findIndex(p => p.name === user.name) === -1) {

      room.participant.push(user.toJSON() as ParticipantDocument)
      await room.save()
    }
    this.client.emit(EMQTTEvent.JOIN_ROOM + response.roomId, response)
    return { joined: true, ...response }
  }
  async emptyBoard(roomId: string, user: ParticipantDocument) {
    const room = await this.roomRepo.findOne({ _id: roomId })
    if (room) {
      if (room.currentWriter === user.id || room.admin === user.id) {
        room.board = ''
        await room.save()
        this.client.emit(EMQTTEvent.EMPTY_BOARD + roomId, { roomId })
        return room.toJSON()
      }
      throw new HttpException("not allowed", HttpStatus.BAD_REQUEST)
    }
    throw new HttpException("not allowed", HttpStatus.BAD_REQUEST)
  }

}
type Task = {
  from: string;
  line: any;
  roomId: string;
}




class DrawDataIngester {
  private queue: queue<Task>;
  constructor(private roomRepo: RoomRepository, private participantRepo: ParticipantRepository) {
    this.queue = fastq(this.worker.bind(this), 1)
    console.log(roomRepo.findOne)
  }
  public addJob(job: Task) {
    this.queue.push(job)
  }
  private async worker(arg: Task, cb: done) {
    console.log('from worker', arg)
    const room = await this.roomRepo.findOne({ _id: arg.roomId })
    const dataURL = room.board
    const canvas = createCanvas(500, 500);
    const ctx = canvas.getContext("2d");
    const img = new Image();
    img.src = dataURL;
    const imgPromise = new Promise((resolve, reject) => {
      img.onload = function () {
        // Vẽ ảnh lên canvas
        img.width = 500;
        img.height = 500;
        ctx.drawImage(img, 0, 0);

        for (let i = 0; i < arg.line.length; i++) {
          const { prevX, prevY, currX, currY } = arg.line[i];
          ctx.beginPath();
          ctx.moveTo(prevX, prevY);
          ctx.lineTo(currX, currY);
          ctx.stroke();
          ctx.closePath();
        }

        resolve(true);
      };

      img.onerror = function () {
        reject("Error loading image.");
      };
    });
    for (let i = 0; i < arg.line.length; i++) {
      const { prevX, prevY, currX, currY } = arg.line[i];
      ctx.beginPath();
      ctx.moveTo(prevX, prevY);
      ctx.lineTo(currX, currY);
      ctx.stroke();
      ctx.closePath();
    }
    const newDataUrl = canvas.toDataURL();
    room.board = newDataUrl
    await room.save()
    cb(null)
  }
}