

export class DrawData {
    line: Pixel[]
    lineWidth: number
    lineColor: string

}
export class Pixel {
    x: number;
    y: number;
}