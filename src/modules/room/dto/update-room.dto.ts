import { CreateRoomDto } from './create-room.dto';

export type UpdateRoomDto = CreateRoomDto
