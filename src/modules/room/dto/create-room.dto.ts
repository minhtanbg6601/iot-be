import { Room } from "src/shared/entities/room.entity";

export type CreateRoomDto = Omit<Room,'id'>
