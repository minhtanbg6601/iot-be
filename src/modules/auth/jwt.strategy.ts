import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';



import { EEnvKey } from 'src/shared/constants/env.constant';
import { EAuthGuard } from 'src/shared/constants/role.constant';
import { ParticipantDocument } from 'src/shared/entities/participant.entity';
import ParticipantRepository from 'src/shared/repos/participant.repository';

@Injectable()
export class UserJwtStrategy extends PassportStrategy(Strategy, EAuthGuard.USER_ROLE) {
    constructor(private userService: ParticipantRepository, configService: ConfigService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: configService.get(EEnvKey.JWT_SECRETE),
        });
    }

    async validate(payload: ParticipantDocument) {
        return await this.userService.findOne({ name: payload.name});
    }
}
