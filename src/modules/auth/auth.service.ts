import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { EEnvKey } from 'src/shared/constants/env.constant';
import { Participant } from 'src/shared/entities/participant.entity';
import ParticipantRepository from 'src/shared/repos/participant.repository';
export const formatJWT = (token: string, expire_in: number) => ({ token, expire_in });

@Injectable()
export class AuthService {
    constructor(
        private participantRepo: ParticipantRepository,
        private jwtService: JwtService,
        private configService: ConfigService,
    ) { }

    async validateUser(wallet_address: string): Promise<Participant> {
        const user = await this.participantRepo.findOne({ wallet_address });
        if (user) {
            return user;
        }
        return null;
    }
    async signUserJWT(user: Participant) {
        try {
            return formatJWT(
                await this.jwtService.signAsync(user, {
                    privateKey: this.configService.get(EEnvKey.JWT_SECRETE),
                    expiresIn:  1286400,
                }),
                Date.now()/1000 +1286400,
            );
        } catch (e) {
            throw new HttpException(
                null,
                HttpStatus.INTERNAL_SERVER_ERROR,
            );
        }
    }
}
