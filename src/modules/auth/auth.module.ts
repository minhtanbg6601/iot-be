import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';
import { Participant, ParticipantSchema } from 'src/shared/entities/participant.entity';
import { RolesGuard } from 'src/shared/guard/roles.guard';
import ParticipantRepository from 'src/shared/repos/participant.repository';

import { AuthService } from './auth.service';
import {  UserJwtStrategy } from './jwt.strategy';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: Participant.name, schema: ParticipantSchema }
        ]),
        PassportModule,
        JwtModule.register({}),
    ],
    providers: [AuthService,ConfigService, UserJwtStrategy,ParticipantRepository, RolesGuard],
    exports: [AuthService],
})
export class AuthModule { }
