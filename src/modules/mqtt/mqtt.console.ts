import { Console, Command } from "nestjs-console";
import { Injectable } from '@nestjs/common'

@Console()
@Injectable()
export class MQTTConsole {
    constructor() {}


    @Command({
        command: 'mqtt',
        description: 'start mqtt handlers',
    })
    async handleMQTT(): Promise<void> {
        console.log('works');
        
    }

}
