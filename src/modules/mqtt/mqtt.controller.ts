import { Controller } from '@nestjs/common';
import { Inject } from '@nestjs/common/decorators';
import { ClientProxy, MessagePattern, Payload } from '@nestjs/microservices';
import { MqttService } from './mqtt.service';


@Controller('math')
export class MqttController {
  constructor(private readonly mqttService: MqttService,
  @Inject('MQTT_SERVICE') private client: ClientProxy) { }
  @MessagePattern('iot-be-hust/room/draw')
  getHello(@Payload() data) {

    console.log(data);
    
   
    return `I got ${JSON.stringify(data)}`

  }
}
