import { Module } from '@nestjs/common';
import { MqttService } from './mqtt.service';
import { MqttController } from './mqtt.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Participant, ParticipantSchema } from 'src/shared/entities/participant.entity';
import { Room, RoomSchema } from 'src/shared/entities/room.entity';
import { MQTTConsole } from './mqtt.console';
import { ClientsModule, Transport } from '@nestjs/microservices';

@Module({
  imports: [

    ClientsModule.register([
      {
        name: 'MQTT_SERVICE',
        transport: Transport.MQTT,
        options: {
          url: process.env.MQTT_URI,
        }
      },
    ]),


    MongooseModule.forFeature([{ name: Participant.name, schema: ParticipantSchema }, { name: Room.name, schema: RoomSchema }])],
  controllers: [MqttController],
  providers: [MqttService, MQTTConsole]
})
export class MqttModule { }
