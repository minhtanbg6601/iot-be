import { Controller, Get, Post, Body, Patch, Param, Delete, Logger } from '@nestjs/common';
import { ParticipantService } from './participant.service';
import { CreateParticipantDto } from './dto/create-participant.dto';
import { UpdateParticipantDto } from './dto/update-participant.dto';
import { UseAuthAndRolesSuperAdmin } from 'src/shared/guard/roles.guard';
import { CurrentUser } from 'src/shared/decorators/user.decorator';


@Controller('participant')
export class ParticipantController {
  constructor(private readonly participantService: ParticipantService) { }
  private readonly logger = new Logger(ParticipantController.name);

  @Post('/signup')
  create(@Body() createParticipantDto: CreateParticipantDto) {
    this.logger.log(createParticipantDto)
    return this.participantService.create(createParticipantDto);
  }
  @Post('/login')
  login(@Body() createParticipantDto: CreateParticipantDto) {

    return this.participantService.login(createParticipantDto);
  }
  @Get()
  @UseAuthAndRolesSuperAdmin()
  findAll( @CurrentUser() user:any) {
    return this.participantService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.participantService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateParticipantDto: UpdateParticipantDto) {
    return this.participantService.update(+id, updateParticipantDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.participantService.remove(+id);
  }
}
