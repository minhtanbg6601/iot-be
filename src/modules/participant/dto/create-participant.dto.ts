import { EUserRole } from "src/shared/guard/roles.guard";

export class CreateParticipantDto {
    name: string;
    password: string;
    role:EUserRole
}