import { Module } from '@nestjs/common';
import { ParticipantService } from './participant.service';
import { ParticipantController } from './participant.controller';
import ParticipantRepository from 'src/shared/repos/participant.repository';
import { MongooseModule } from '@nestjs/mongoose/dist/mongoose.module';
import { Participant, ParticipantSchema } from 'src/shared/entities/participant.entity';
import { AuthModule } from '../auth/auth.module';

@Module({
  imports: [MongooseModule.forFeature([{ name: Participant.name, schema: ParticipantSchema }]),AuthModule],
  controllers: [ParticipantController],
  providers: [ParticipantService, ParticipantRepository]
})
export class ParticipantModule { }
