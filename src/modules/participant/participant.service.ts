import { HttpException, Injectable } from '@nestjs/common';
import { CreateParticipantDto } from './dto/create-participant.dto';
import ParticipantRepository from 'src/shared/repos/participant.repository';
import { UpdateParticipantDto } from './dto/update-participant.dto';
import { EUserRole } from 'src/shared/guard/roles.guard';
import { AuthService } from '../auth/auth.service';

@Injectable()
export class ParticipantService {
  constructor(private participantRepo: ParticipantRepository, private auth: AuthService) { }
  async create(createParticipantDto: CreateParticipantDto) {
    createParticipantDto.role = EUserRole.USER;
    const response=await this.participantRepo.createOne(createParticipantDto);
    return { ...response.toJSON(), auth: await this.auth.signUserJWT(response.toJSON()) }

  }
  async login(loginDto: CreateParticipantDto) {
    console.log('====================================');
    console.log(loginDto);
    console.log('====================================');
    const response = await this.participantRepo.findOne(loginDto);
    if (!response) {
      throw new HttpException("not found", 404)
    }
    return { ...response.toJSON(), auth: await this.auth.signUserJWT(response.toJSON()) }
  }
  findAll() {
    return this.participantRepo.findAll();
  }

  findOne(_id: number) {
    return this.participantRepo.findOne({ _id });
  }

  update(id: number, updateParticipantDto: UpdateParticipantDto) {
    return `This action updates a #${id} participant`;
  }

  remove(id: number) {
    return this.participantRepo.deleteOne(id);
  }
}
