import { ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';
import { EAuthGuard } from '../constants/role.constant';



import { Participant } from '../entities/participant.entity';
import { IS_PUBLIC_KEY } from './roles.guard';

@Injectable()
export class JwtAuthGuard extends AuthGuard([EAuthGuard.USER_ROLE]) {
    constructor(private reflector: Reflector) {
        super();
    }

    canActivate(context: ExecutionContext) {
        const isPublic = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_KEY, [
            context.getHandler(),
            context.getClass(),
        ]);

        if (isPublic) return true;
        return super.canActivate(context);
    }

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    handleRequest(err, user: Participant) {
        if (err || !user) {
            throw err || new UnauthorizedException();
        }
        return user;
    }
}

