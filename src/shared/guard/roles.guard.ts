import { applyDecorators, CanActivate, ExecutionContext, Injectable, SetMetadata, UseGuards } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';
import { EAuthGuard } from '../constants/role.constant';

export const ROLE_KEY = 'iot-role'
export const IS_PUBLIC_KEY = 'isPublic'
export enum EUserRole {
    USER = 'user'
}

@Injectable()
export class RolesGuard implements CanActivate {
    constructor(private reflector: Reflector) { }

    canActivate(context: ExecutionContext): boolean {
        const requiredRoles = this.reflector.getAllAndOverride<EUserRole[]>(ROLE_KEY, [
            context.getHandler(),
            context.getClass(),
        ]);
        const isPublicApi = this.reflector.getAllAndOverride<EUserRole[]>(IS_PUBLIC_KEY, [
            context.getHandler(),
            context.getClass(),
        ]);

        if (isPublicApi) return true;


        if (!requiredRoles || !requiredRoles.length) {
            return true;
        }

        const { user } = context.switchToHttp().getRequest();
        if (!user) return false;

        return requiredRoles.some(role => user.role === role);
    }
}

export const Roles = (role: EUserRole[]) => SetMetadata(ROLE_KEY, role);



export function UseAuthAndRolesSuperAdmin() {
    return applyDecorators(
        UseGuards(AuthGuard(EAuthGuard.USER_ROLE), RolesGuard),
        Roles([EUserRole.USER])
    );
}