
const PREFIX = 'iot-be-hust/'
export const EMQTTEvent = {
    CREATE_ROOM: PREFIX + 'create_room',
    UPDATE_ROOM: PREFIX + 'update_room',
    DELETE_ROOM: PREFIX + 'delete_room',


    DRAW: PREFIX + 'room/draw/',
    CHOOSEN:PREFIX+'room/choosen/',
    EMPTY_BOARD:PREFIX +'room/clear_board/',

    // users
    JOIN_ROOM: PREFIX + 'participant/join/',
    EXIT_ROOM: PREFIX + 'participant/exit/',

    RAISE_HAND: PREFIX + 'parcitipant/raise_hand/',
    KICK: PREFIX + 'participant/kick/'
};
