import { InjectModel } from "@nestjs/mongoose";
import { FilterQuery, Model } from "mongoose";
import { CreateParticipantDto } from '../../modules/participant/dto/create-participant.dto';
import { Participant, ParticipantDocument } from "../entities/participant.entity"
import { Controller, Get, Logger } from '@nestjs/common';

export default class ParticipantRepository {
    constructor(@InjectModel(Participant.name) private participantModel: Model<ParticipantDocument>) { }
    private readonly logger = new Logger(ParticipantRepository.name);
    findOne(dto: FilterQuery<Participant>) {
        // TODO: write dto for participant filter
        return this.participantModel.findOne(dto)
    }
    findAll() {
        return this.participantModel.find()
    }
    async createOne(newParticipant: CreateParticipantDto) {
        return this.participantModel.create(newParticipant)
    }
    deleteOne(id: number) {
        return this.participantModel.findByIdAndDelete(id)
    }
}