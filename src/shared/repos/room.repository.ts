import { InjectModel } from "@nestjs/mongoose";
import { FilterQuery, Model } from "mongoose";
import { CreateRoomDto } from "src/modules/room/dto/create-room.dto";
import { UpdateRoomDto } from "src/modules/room/dto/update-room.dto";
import { Room } from "../entities/room.entity";


export default class RoomRepository {
    constructor(@InjectModel(Room.name) private roomModel: Model<Room>) { }

    findOne(dto: FilterQuery<Room>) {
        return this.roomModel.findOne(dto)
    }
    findMany(dto: FilterQuery<Room>) {
        return this.roomModel.find(dto)
    }
    createOne(newRoom: CreateRoomDto) {
        return this.roomModel.create(newRoom)
    }
    updateOne(id: string, updatedRoom: UpdateRoomDto) {
        return this.roomModel.findByIdAndUpdate(id, updatedRoom)
    }
    deleteOne(id: string) {
        return this.roomModel.findByIdAndDelete(id);
    }
}