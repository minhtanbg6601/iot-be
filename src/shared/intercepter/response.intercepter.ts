import { CallHandler, ExecutionContext, HttpStatus, Injectable, NestInterceptor } from '@nestjs/common';
import { ArgumentsHost, Catch, ExceptionFilter } from '@nestjs/common';

import { Response } from 'express';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


export const defaultResponse: IResponse<[]> = {
    success: true,
    statusCode: HttpStatus.OK,
    message: '',
    data: null,
    error: [],
};

export interface IResponse<T> {
    statusCode?: HttpStatus;
    data?: T;
    _metadata?: {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        [key: string]: any;
    };
    message?: string | null;
    messageDetail?: any;
    success?: boolean;
    error?: any[];
}
export function createResponse(data: any) {
    let metadata;

    if (data?._metadata) {
        metadata = {
            ...data._metadata,
            timestamp: new Date(),
        };
        delete data._metadata;
    } else {
        metadata = {};
    }
    return {
        success: true,
        statusCode: data?.statusCode ? data.statusCode : HttpStatus.OK,
        data: data?.data || data || [],
        message: "ok",
    };
}
@Injectable()
export class ResponseTransformInterceptor<T> implements NestInterceptor<T, IResponse<T>> {
    intercept(context: ExecutionContext, next: CallHandler): Observable<IResponse<T>> {
        return next.handle().pipe(
            map(data => {
                const ctx = context.switchToHttp();
                const response = ctx.getResponse<Response>();
                const responseData = createResponse(data);
                if (responseData?.statusCode) {
                    response.status(responseData.statusCode);
                }
                return createResponse(data);
            }),
        );
    }
}




@Catch()
export class UnknownExceptionsFilter implements ExceptionFilter {

    catch(exception: unknown, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();

        const defaultResponse: IResponse<any> = {
            data: null,
            error: [],
            statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            message: typeof exception === 'object' && exception?.message ? exception.message : '',
            success: false,
        };
        response.status(defaultResponse.statusCode).json(defaultResponse);
    }
}
