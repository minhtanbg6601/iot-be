import { Prop, Schema } from "@nestjs/mongoose";
import { SchemaFactory } from "@nestjs/mongoose/dist/factories";
import { HydratedDocument } from "mongoose";

export interface ISession {
    roomId: string;
    data: Object;
    member: Object;
    lastTimestamp: number;
}
export type SessionDocument = HydratedDocument<Session>;

@Schema({})
export class Session implements ISession {
    @Prop({ required: true })
    roomId: string;
    @Prop({ required: true })
    data: Object;
    @Prop({ required: true })
    member: Object;
    @Prop({ required: true })
    lastTimestamp: number;

}
export const SessionSchema = SchemaFactory.createForClass(Session);
