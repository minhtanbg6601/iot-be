import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { HydratedDocument } from "mongoose";
import { EUserRole } from "../guard/roles.guard";

export interface IParticipant {
    name: string;
    password: string;
    role: EUserRole;
}
export type ParticipantDocument = HydratedDocument<Participant>
@Schema({
    versionKey: false,
    virtuals: true,
})

export class Participant implements IParticipant {
    @Prop({ required: true, default: '', type: String, unique: true })
    name: string;
    @Prop({ required: true, default: '', type: String })
    password: string;

    @Prop({ required: true, default: '', type: String })
    role: EUserRole;
}
export const ParticipantSchema = SchemaFactory.createForClass(Participant);

ParticipantSchema.index({ name: 1 }, { unique: true })