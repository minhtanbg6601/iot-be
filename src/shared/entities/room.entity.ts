import { Prop, Schema } from "@nestjs/mongoose";
import { SchemaFactory } from "@nestjs/mongoose/dist/factories";
import { HydratedDocument } from "mongoose";
import { Participant, ParticipantDocument } from "./participant.entity";

export interface IRoom {
    name: string;
    limit: number;
    admin: string;
    current: number;
    status: ERoomStatus;
    currentWriter: string;
    board : string;
    participant: ParticipantDocument[];
}
export type RoomDocument = HydratedDocument<Room>;
export enum ERoomStatus {
    OFFLINE = 'offline',
    ONLINE = 'online',
}
@Schema({})
export class Room implements IRoom {
    @Prop({ required: true })
    name: string;
    @Prop({ required: true })
    limit: number;
    @Prop({ required: true })
    admin: string;
    @Prop({ required: true, default: 0 })
    current: number;
    @Prop({ required: true, default: ERoomStatus.OFFLINE })
    status: ERoomStatus;
    @Prop({ required: false, default: '' })
    currentWriter: string;
    @Prop({ required: false, default: '' })
    board: string;
    @Prop({ required: true, default: [] })
    participant: ParticipantDocument[]

}
export const RoomSchema = SchemaFactory.createForClass(Room);
