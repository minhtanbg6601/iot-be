import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RoomModule } from './modules/room/room.module';
import { ParticipantModule } from './modules/participant/participant.module';
import { MongooseModule } from '@nestjs/mongoose';
import { MqttModule } from './modules/mqtt/mqtt.module';
import { ConfigModule } from '@nestjs/config';
import { ConsoleModule } from 'nestjs-console';
import { AuthModule } from './modules/auth/auth.module';

@Module({
  imports: [ConfigModule.forRoot(), MongooseModule.forRoot(process.env.DB_URI), ConsoleModule, AuthModule, RoomModule, ParticipantModule, MqttModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
