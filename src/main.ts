import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import {MicroserviceOptions, Transport} from '@nestjs/microservices'
import { ResponseTransformInterceptor, UnknownExceptionsFilter } from './shared/intercepter/response.intercepter';
import { ConfigService } from '@nestjs/config';
import { EEnvKey } from './shared/constants/env.constant';
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalInterceptors(new ResponseTransformInterceptor());
  app.useGlobalFilters(new UnknownExceptionsFilter());
  const cfService=await app.get(ConfigService)
  await app.enableCors()
  await app.listen(cfService.get('PORT'));
  const mqtt = await NestFactory.createMicroservice<MicroserviceOptions>(AppModule, {
    transport: Transport.MQTT,
    options: {
      url: cfService.get(EEnvKey.MQTT_URI)
      },
  });
  await mqtt.listen()
}
bootstrap();
